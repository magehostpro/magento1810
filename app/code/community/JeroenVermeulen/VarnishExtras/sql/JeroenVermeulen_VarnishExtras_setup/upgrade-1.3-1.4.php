<?php
/** @var $this Mage_Catalog_Model_Resource_Setup */
$this->startSetup();
Mage::getSingleton('adminhtml/session')->addNotice( sprintf( "Setup file '%s/%s' executed.", basename(dirname(__FILE__)), basename(__FILE__) ) );
$this->endSetup();