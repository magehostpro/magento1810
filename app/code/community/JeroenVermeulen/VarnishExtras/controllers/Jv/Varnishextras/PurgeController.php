<?php

class JeroenVermeulen_VarnishExtras_Jv_Varnishextras_PurgeController extends Mage_Adminhtml_Controller_Action
{

    // URL:  http://[MAGROOT]/admin/jv_varnishextras_purge/key/###########/
    // URL:  http://[MAGROOT]/admin/jv_varnishextras_purge/index/key/###########/
    // If "storecode in url" is enabled there will be "/admin/admin" before "/jv_varnishextras_purge"
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
        /**
         * Layout will be chosen by @see Mage_Core_Controller_Varien_Action::addActionLayoutHandles
         * Layout file:        /app/design/adminhtml/default/default/layout/JeroenVermeulen/VarnishExtras.xml
         * Item in that file:  adminhtml_jv_varnishextras_purge_index
         */
        // Enable to debug layout XML:
        // header( 'Content-Type: text/xml' ); echo $this->getLayout()->getXmlString(); exit;
    }

    // URL:  http://[MAGROOT]/admin/jv_varnishextras_purge/post/key/###########/
    // If "storecode in url" is enabled there will be "/admin/admin" before "/jv_varnishextras_purge"
    public function postAction()
    {
        $oSession = Mage::getSingleton( 'core/session' );
        $aPost    = $this->getRequest()->getPost();
        $sUrl     = trim( $aPost['purgeform']['url'] );
        $sMethod  = empty( $aPost['purgeform']['method'] ) ? 'exact' : $aPost['purgeform']['method'];
        if ( !preg_match('|https?://|',$sUrl) )
        {
            $oSession->addError( $this->__('Please use a full URL, start with "http://"') );
        }
        $aUrlParts = parse_url( $sUrl );
        if ( false === $aUrlParts || empty( $aUrlParts['host'] ) or empty( $aUrlParts['path'] ) )
        {
            $oSession->addError( $this->__('Invalid URL: <b>%s</b>.',$sUrl) );
            $oSession->setData( 'JeroenVermeulen_VarnishExtras_PurgeURL', '' );
        }
        else
        {
            Mage::log( sprintf( 'JeroenVermeulen_VarnishExtras_PurgeController %s "%s"'
                              , $sMethod
                              , $sUrl
                              )
                     , Zend_Log::INFO
                     , 'turpentine.log'
                     );
            $aSockets = Mage::helper( 'turpentine/varnish' )->getSockets();
            if ( empty( $aSockets ) )
            {
                $msg = $this->__( 'No Varnish servers found in Turpentine configuration.' );
                $oSession->addError( $msg );
                Mage::log( $msg, Zend_Log::WARN, 'turpentine.log' );
            }
            else
            {
                $result = array();
                /** @var Nexcessnet_Turpentine_Model_Varnish_Admin_Socket $socket */
                foreach( $aSockets as $socket )
                {
                    $socketName = $socket->getConnectionString();
                    try
                    {
                        $sPathMatch = $aUrlParts['path'];
                        $sPathMatch = rtrim( $sPathMatch, '/' );
                        $sPathMatch = '^' . $sPathMatch;
                        if ( 'wildcard' == $sMethod )
                        {
                            $sPathMatch .= '.*';
                        }
                        else
                        {
                            $sPathMatch .= '/?($|\?|#)';
                        }
                        $aSockReturn = $socket->ban( 'obj.http.X-Varnish-Host', '==', $aUrlParts['host'], '&&', 'obj.http.X-Varnish-URL', '~', $sPathMatch );
                        if (   !empty($aSockReturn['code'])
                           and ( 200 <= $aSockReturn['code'] )
                           and ( 300 > $aSockReturn['code'] )
                           )
                        {
                            $result[$socketName] = true;
                        }
                        else
                        {
                            $socketReturnCode = empty($aSockReturn['code']) ? '[EMPTY]' : $aSockReturn['code'];
                            $socketReturnMsg  = empty($aSockReturn['text']) ? '' : $aSockReturn['text'];
                            $result[$socketName] = sprintf( 'ERROR code %s: %s', $socketReturnCode, $socketReturnMsg );
                        }
                    }
                    catch( Exception $e )
                    {
                        $result[$socketName] = $e->getMessage();
                        continue;
                    }
                }
                foreach( $result as $name => $value )
                {
                    if( $value === true )
                    {
                        if ( 'wildcard' == $sMethod )
                        {
                            $msg = $this->__( 'All URLs starting with %s have been purged on %s.'
                                            , '<span style="color:#FF6600">'.$sUrl.'</span>'
                                            , $name
                                            );
                        }
                        else
                        {
                            $sLink = sprintf( '<a href="%s" target="_blank">%s</a>', $sUrl, $sUrl );
                            $msg = $this->__( 'The URL %s has been purged on %s.', $sLink, $name );
                        }
                        Mage::log( $msg, Zend_Log::INFO, 'turpentine.log' );
                        $oSession->addSuccess( $msg );
                    }
                    else
                    {
                        $msg = $this->__( 'Error purging URL %s on %s: %s', $sUrl, $name, $value );
                        Mage::log( $msg, Zend_Log::ERR, 'turpentine.log' );
                        $oSession->addError( $msg );
                    }
                }
            }
            $oSession->setData( 'JeroenVermeulen_VarnishExtras_PurgeURL', $sUrl );
            $oSession->setData( 'JeroenVermeulen_VarnishExtras_PurgeMethod', $sMethod );
        }
        $this->_redirect( '*/*/' ); // Redirect to index action
    }

}
