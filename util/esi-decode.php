<html>
<head>
    <title>ESI Request Decoder</title>
</head>
<body>
<?php
// You might need to fix the path to your app/Mage.php below.
require_once dirname(__FILE__).'/../app/Mage.php';

Mage::app();

$data = ( empty($_REQUEST['data']) ) ? '' : $_REQUEST['data'];
?>
<form method="post">
    ESI Data or URL: <br />
    <textarea rows="10" cols="100" name="data"><?php echo $data; ?></textarea><br />
    <input type="submit" value="    DECODE    " />
</form>
<?php
if ( $data ) {
    $processData = $data;
    if ( preg_match('|data/([\w\.\-]+=*)|', $data, $matches) ) {
        $processData = $matches[1];
    }
    $dataHelper = Mage::helper( 'turpentine/data' );
    $esiDataArray = $dataHelper->thaw( $processData );
    echo '<pre style="background-color: #ffff90">';
    var_export( $esiDataArray );
    echo '</pre>';
}
?>
</body>
</html>